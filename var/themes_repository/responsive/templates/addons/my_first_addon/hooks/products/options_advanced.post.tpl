<div class="control-group">
    <label class="control-label" for="elm_date_release_date">{__("my_first_addon.release_date")}: {$product.release_date|date_format:"`$settings.Appearance.date_format`"}</label>
</div>
