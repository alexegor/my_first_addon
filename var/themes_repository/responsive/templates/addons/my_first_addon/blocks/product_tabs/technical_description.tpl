{** block-description:my_first_addon.technical_description **}
{if $product.technical_description}
    <div {live_edit name="product:technical_description:{$product.product_id}"}>{$product['technical_description']|strip_tags}</div>
{/if}
