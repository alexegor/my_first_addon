{include file="common/subheader.tpl" title=__("my_first_addon.my_first_addon") target="#my_first_addon_product_setting"}
    <div id="my_first_addon_product_setting" class="in collapse">
    	<fieldset>
            <div class="control-group">
                <label class="control-label" for="ean_code">{__("my_first_addon.ean_code")}:</label>
                <div class="controls">
                    <input type="text" id="ean_code" name="product_data[ean_code]" value="{$product_data.ean_code}" class="input-large" size="10" />
                </div>
            </div>
            <div class="control-group {$no_hide_input_if_shared_product}">
                <label class="control-label" for="technical_description">{__("my_first_addon.technical_description")}:</label>
                <div class="controls">
                    <textarea id="elm_product_technical_description" name="product_data[technical_description]" cols="55" rows="2" class="cm-wysiwyg input-large">{$product_data.technical_description}</textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="special_edition">{__("my_first_addon.special_edition")}:</label>
                <div class="controls">
                    <input type="hidden" name="product_data[special_edition]" value="N" />
                    <input type="checkbox" name="product_data[special_edition]" id="product_special_edition" value="Y" {if $product_data.special_edition == "Y"}checked="checked"{/if} />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="release_date">{__("my_first_addon.release_date")}:</label>
                <div class="controls">
                    {include file="common/calendar.tpl" date_id="release_date" date_name="product_data[release_date]" date_val=$product_data.release_date|default:$smarty.const.TIME start_year=$settings.Company.company_start_year}
                </div>
            </div>
        </fieldset>
    </div>