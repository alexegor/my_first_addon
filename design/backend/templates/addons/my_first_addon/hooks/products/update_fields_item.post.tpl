{if $type == "T"}
    <div class="correct-picker-but">
    {if $field == "release_date"}
        {include file="common/calendar.tpl" date_id="date_release_date_holder_`$product.product_id`" date_name="$name[`$product.product_id`][$field]" date_val=$product.$field start_year=$settings.Company.company_start_year}
    {/if}
    </div>
{/if}